import { MongoClient } from "mongodb";
console.log("hello i am running");
// Replace the uri string with your connection string.
const uri: string = process.env.MONGO_URI;

export default defineEventHandler(async (event) => {
  const client = new MongoClient(uri);
  async function run() {
    try {
      const database = client.db("tirestores");
      const storesCollection = database.collection("stores");
      // Query for a movie that has the title 'Back to the Future'
      const cursor = await storesCollection.find({ "address.region.name": "British Columbia" }).toArray();
      console.log(typeof cursor);

      return cursor;
    } catch {
      console.dir;
    } finally {
      // Ensures that the client will close when you finish/error
      await client.close();
    }
  }
  console.log("now I am running from server");
  let bcdata = await run();
  // bcdata.forEach((store) => {
  //   console.log(store.id);
  // });
  return bcdata;
});
