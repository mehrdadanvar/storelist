export default defineEventHandler((event) => {
  return {
    url: event.req.url,
    data: event.context.params,
  };
});
