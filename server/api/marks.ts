import { MongoClient } from "mongodb";
console.log("hello i am running from rona");
// Replace the uri string with your connection string.
const uri: string = process.env.MONGO_URI;

export default defineEventHandler(async (event) => {
  const nclient = new MongoClient(uri);
  async function run() {
    try {
      const database = nclient.db("marksdb");
      const storesCollection = database.collection("marks");
      // Query for a movie that has the title 'Back to the Future'
      const cursor = await storesCollection.find().toArray();
      console.log(typeof cursor);

      return cursor;
    } catch {
      console.dir;
    } finally {
      // Ensures that the client will close when you finish/error
      await nclient.close();
    }
  }
  console.log("now I am running and I think it is too early");
  let marksdata = await run();
  //   console.log(`store data length from backend is ${storesdata.length}`);
  return marksdata;
});
